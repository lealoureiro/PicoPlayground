#include <stdio.h>
#include "pico/stdlib.h"


const uint RED_LED = 13;
const uint BLUE_LED = 14;
const uint GREEN_LED = 15;

int main() {

    gpio_init(RED_LED);
    gpio_init(BLUE_LED);
    gpio_init(GREEN_LED);

    gpio_set_dir(RED_LED, GPIO_OUT);
    gpio_set_dir(BLUE_LED, GPIO_OUT);
    gpio_set_dir(GREEN_LED, GPIO_OUT);

    bool redLedState = false;
    bool blueLedState = false;
    bool greenLedState = false;

    stdio_init_all();

    while (true) {

        int16_t led = getchar_timeout_us(1);

        if (led == 'r') {
            redLedState = !redLedState;
            gpio_put(RED_LED, redLedState);
        } else if (led == 'g') {
            greenLedState = !greenLedState;
            gpio_put(GREEN_LED, greenLedState);
        } else if (led == 'b') {
            blueLedState = !blueLedState;
            gpio_put(BLUE_LED, blueLedState);
        } else if (led == PICO_ERROR_TIMEOUT) {
            // do nothing
        } else {
            printf("Unknown command!\n");
        }

    }

    return 0;
}