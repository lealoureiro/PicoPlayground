#include <stdio.h>
#include "pico/stdlib.h"
#include "hardware/uart.h"
#include "hardware/irq.h"


#define UART_ID uart0
#define BAUD_RATE 38400
#define UART_TX_PIN 0
#define UART_RX_PIN 1
#define DATA_BITS 8
#define STOP_BITS 1
#define PARITY    UART_PARITY_NONE


void on_uart_rx();

int main() {

    uart_init(UART_ID, BAUD_RATE);

    gpio_set_function(UART_TX_PIN, GPIO_FUNC_UART);
    gpio_set_function(UART_RX_PIN, GPIO_FUNC_UART);

    //uart_set_fifo_enabled(UART_ID, false);



/*

    int UART_IRQ = UART_ID == uart0 ? UART0_IRQ : UART1_IRQ;
    irq_set_exclusive_handler(UART_IRQ, on_uart_rx);
    irq_set_enabled(UART_IRQ, true);
    uart_set_irq_enables(UART_IRQ, true, false);
    */

    stdio_init_all();

    while (1) {
        if (uart_is_readable(UART_ID)) {
        uint8_t data = uart_getc(UART_ID);
        printf("DATA: %d\n", data);
        } else {
            printf("NO DATA!\n");
        }
        sleep_ms(100);
    }

}
